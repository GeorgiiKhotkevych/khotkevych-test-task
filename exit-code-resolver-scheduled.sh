#!/bin/bash

RESULT_VAR=$(jq . $CI_PROJECT_DIR/result/cont-sast-report.json |jq . $CI_PROJECT_DIR/result/cont-sast-report.json | jq -r '.results[] | .extra | .severity')

if [[ -z $RESULT_VAR ]];
then
    echo 'TEST PASSED!'
    exit 0
else
    echo 'You screwed up again!'
    exit 1
fi