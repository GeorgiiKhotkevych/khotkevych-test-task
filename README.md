# SoftServe test project

Using the Gitlab CI capabilities, build a Docker image that contains Python3 code. Analise the code with a Semgrep tool for vulnerabilities.
Parse the Semgrep's report and show it in the Gitlab CI job output.

## Description
1. Create a Gitlab.com public repository with a simple Python 3 code that either contains a vulnerability that Semgrep is able to find or create a custom Semgrep rule that will "catch" a fake vulnerability in the code in the repository.
2. Create a Gitlab CI that looks like the following:
1. "sast": Test the python codebase with Semgrep. The report should be exported as JSON.
2. "report": Analyze the JSON report artifact from the "sast" step and show the summary of the existing vulnerabilities in the output in a text format (parse JSON with python code or jq).
1. If a report shows HIGH or CRITICAL levels of vulnerabilities - fail the step, but not the whole Pipeline.
3. "build_develop": Build the "develop" docker image from the alpine base image that contains the code from the repo in the "/app" directory. Push it to the Container Registry.
4. "build_latest": If the Semgrep report doesn't contain HIGH or CRITICAL levels of vulnerabilities - create a "latest" docker image from the alpine base image that includes the code from the repo in the "/app" directory. Push it to the Container Registry.
5. "build_tag": Create a "%tag_name%" docker image from the alpine base image that contains the code from the repo in the "/app" directory. Push it to the Container Registry.
1. This step should run only when the tag is created.
2. "Develop" docker image should not be created/updated.
6. "sast_latest": create a step that will run only on a scheduled basis (e.g. once per day) that will analyze the code in the "/app" directory of the "latest" docker image with a Semgrep tool. (similar as for "sast" step).
7. "report_latest": Analise the report of the "sast_latest" step similar to the "sast" one.
1. The whole Pipeline should fail if the report from the "sast_latest" step shows HIGH or CRITICAL levels of vulnerabilities.

## Project status

