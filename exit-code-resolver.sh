#!/bin/bash

TEST_VAR=$(jq . result/gl-sast-report.json |jq -r '.vulnerabilities[] | .severity')

if [ $TEST_VAR == "High" ]
then
    BUILD_VAR='develop'
elif [ $TEST_VAR == "Critical" ]
then
    BUILD_VAR='develop'
else
    BUILD_VAR='latest'
fi

if [ $TEST_VAR == "High" ]
then
    echo $TEST_VAR 'severity vulnerabilities were found. Next step will build' $BUILD_VAR 'docker image.'
    echo "BUILD_NAME='Build develop'" >> build_type.env
    exit 1
elif [ $TEST_VAR == "Critical" ]
then
    echo $TEST_VAR 'severity vulnerabilities were found. Next step will build' $BUILD_VAR 'docker image.'
    echo "BUILD_NAME='Build develop'" >> build_type.env
    exit 1
else
    echo 'High or critical severity vulnerabilities were not found. Next step will build' $BUILD_VAR 'docker image.'
    echo "BUILD_NAME='Build latest'" >> build_type.env
    exit 0
fi

exec $SHELL -i